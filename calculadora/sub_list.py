import numbers
from typing import List
from .log_texto import log


def sub_list(list: List):
    resultado = list[0]
    for num in list[1:]:
        if isinstance(num, numbers.Number):
            resultado -= num
        else:
            raise ValueError("Algum intem da lista não é um valor numerico")
        
    log('Subtrair lista', str(list), None, resultado)
    return resultado
