import numbers


def funcao_x(numero: numbers.Number):
    if isinstance(numero, numbers.Number):
        y = numero ** 2 + (3 * numero) + 2
        return y
    else:
        raise ValueError('Valor de x deve ser um numero')


def montar_polinomio(a: numbers.Number, b: numbers.Number, c: numbers.Number, valor_x: numbers.Number):
    if isinstance((a, b, c, valor_x), numbers.Number):
        y = a * (valor_x ** 2) + b * valor_x + c
        return y
    else:
        raise ValueError('Valor de x deve ser um numero')