import math
from .log_texto import log


def logaritmo(number):
    
    try:
        response = math.log(number)
        log('Logaritmo', str(number), str(10), response)
        return math.log(number)
    except ValueError:
        return "Erro! Verifique!"
