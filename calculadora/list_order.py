from typing import List
from .log_texto import log


def list_order(lista: List):
    r = lista
    lista.sort()
    log('Ordenar lista', str(r), None, lista)
    return lista
