def log(operacao, operando1, operando2, resultado):
    data = f"""    ==================
    {operacao}
    operando_1 = {operando1}
    operando_2 = {operando2}
    resultado = {resultado}
    ==================
    """
    escrever_no_log(str(data))


def escrever_no_log(data: str):
    with open('log.txt', 'a') as arquivo:
        # Escreve algo no arquivo
        arquivo.write(data + "\n")
