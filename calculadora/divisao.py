import numbers
from .log_texto import log


def divisao(number1: numbers, number2: numbers):
    try:
        resultado = number1 / number2
        log('Divisão', number1, number2, resultado)
        return resultado
    except ValueError:
        return "Erro! Verifique!"
