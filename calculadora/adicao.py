import numbers
from .log_texto import log


def adicao(operacao_a: numbers, operacao_b: numbers):
    if isinstance(operacao_a, numbers.Number) and isinstance(operacao_b, numbers.Number):
        resultado = operacao_a + operacao_b
        log('Soma', operacao_a, operacao_b, resultado)
        return resultado
    else:
        raise ValueError("Erro, algum dos valores indicados não é numerico")
