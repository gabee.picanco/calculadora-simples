import math
import numbers
from .log_texto import log


def fatorial(operacao_a: numbers):
    if isinstance(operacao_a, int) and operacao_a > 0:
        response = math.factorial(operacao_a)
        log('Logaritmo', str(operacao_a), None, response)
        return response
    else:
        raise ValueError("Valor invalido. Não existe fatorial de numeros não inteiros ou caracteres não numericos.")
