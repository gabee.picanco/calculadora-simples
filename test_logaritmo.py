from calculadora.logaritmo import logaritmo

def test_log_zero():
    assert logaritmo(0) == "Erro! Verifique!"

def test_log_um():
    assert logaritmo(1) == 0

def test_log_number():
    assert logaritmo(10) == 2.302585092994046