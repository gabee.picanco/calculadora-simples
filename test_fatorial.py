import pytest

from calculadora.fatorial import fatorial


def test_fatorial_inteiro():
    assert fatorial(3) == 6


def test_fatorial_float():
    with pytest.raises(ValueError) as e:
        fatorial(3.5)
    assert str(e.value) == "Valor invalido. Não existe fatorial de numeros não inteiros ou caracteres não numericos."


def test_fatorial_caracteres_nao_numerico():
    with pytest.raises(ValueError) as e:
        fatorial('a')
    assert str(e.value) == "Valor invalido. Não existe fatorial de numeros não inteiros ou caracteres não numericos."


def test_fatorial_numeros_negativos():
    with pytest.raises(ValueError) as e:
        fatorial(-4)
    assert str(e.value) == "Valor invalido. Não existe fatorial de numeros não inteiros ou caracteres não numericos."
