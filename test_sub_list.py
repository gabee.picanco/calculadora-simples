import pytest

from calculadora.sub_list import sub_list


def test_list_randon():
    assert sub_list([8, 7, 9]) == -8


def test_list_zeros():
    assert sub_list([0, 0, 0]) == 0


def test_list_float():
    assert sub_list([1.0, 2.0]) == -1.0


def test_list_letras():
    with pytest.raises(ValueError) as e:
        sub_list([1, 'a', '3'])
    assert str(e.value) == "Algum intem da lista não é um valor numerico"
