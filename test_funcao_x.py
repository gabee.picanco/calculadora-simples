import pytest

from calculadora.funcao_quadradica import funcao_x


def test_funcao_x():
    valor = funcao_x(2)
    assert valor == 12


def test_zero_1():
    valor = funcao_x(-1)
    assert valor == 0


def test_zero_2():
    valor = funcao_x(-2)
    assert valor == 0


def test_fracao():
    valor = funcao_x(1/2)
    assert valor == 3.75


def test_caracter_nao_numerico():
    with pytest.raises(ValueError) as e:
        funcao_x('abc')

    assert str(e.value) == "Valor de x deve ser um numero"


def test_caracter_numerico_string():
    with pytest.raises(ValueError) as e:
        funcao_x('1')
    assert str(e.value) == "Valor de x deve ser um numero"
