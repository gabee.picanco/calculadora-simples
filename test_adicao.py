import pytest

from calculadora.adicao import adicao


def test_add_numero():
    assert adicao(1, 2) == 3


def test_add_numero2():
    assert adicao(5, 5) == 10


def test_add_float_number():
    assert adicao(3.5, 33) == 36.5


def test_add_nao_numero():
    with pytest.raises(ValueError) as e:
        adicao('a', 1)
    assert str(e.value) == "Erro, algum dos valores indicados não é numerico"
