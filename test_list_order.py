from calculadora.list_order import list_order


def test_lista_ordem_crescente():
    assert list_order([1, 2, 3]) == [1, 2, 3]


def test_lista_ordem_decresente():
    assert list_order([3, 2, 1]) == [1, 2, 3]


def test_lista_igual():
    assert list_order([0, 0, 0]) == [0, 0, 0]


def test_lista_unitária():
    assert list_order([2]) == [2]


def test_lista_vazia():
    assert list_order([]) == []
